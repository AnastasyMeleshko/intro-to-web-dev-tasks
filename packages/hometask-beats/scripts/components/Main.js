import Collections from "./Collections/Collections";
import Feature from "./Feature/Feature";
import Products from "./Products/Products";
import Boxing from "./Boxing/Boxing";
import Subscribe from "./Subscribe/Subscribe";

export default function Main() {
  const main = document.createElement('main');
  main.classList.add("main");
  main.append(Collections());
  main.append(Feature());
  main.append(Products());
  main.append(Boxing());
  main.append(Subscribe());

  return main;
}






