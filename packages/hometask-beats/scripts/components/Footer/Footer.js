import "./Footer.css";

export default function Footer() {
  const footer = document.createElement('footer');
  footer.classList.add("footer");

  footer.innerHTML=`
     <div class="footer-section container">
        <div class="footer-logo"><a href="#"><img src="../../../assets/icons/logo.svg" alt="logo"></a></div>
        <ul class="footer-links">
         <li class="footer-links-item"><a href="#home">Home</a></li>
         <li class="footer-links-item"><a href="#about">About</a></li>
         <li class="footer-links-item"><a href="#product">Product</a></li>
       </ul>
       <ul class="footer-social-links">
         <li class="footer-social-links-item"><img src="../../../assets/icons/instagram.svg" alt="instagram-icon"></li>
         <li class="footer-social-links-item"><img src="../../../assets/icons/twitter.svg" alt="twitter-icon"></li>
         <li class="footer-social-links-item"><img src="../../../assets/icons/facebook.svg" alt="facebook-icon"></li>
       </ul>
     </div>
  `;

  return footer;
}
