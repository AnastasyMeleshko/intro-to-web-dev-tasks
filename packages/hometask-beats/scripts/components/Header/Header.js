import "./header.css";

export default function Header() {
  const header = document.createElement('header');
  header.classList.add("header");

  header.innerHTML=`
     <div class="container header__container" id="home">
        <div class="header__menu">
          <div class="header__logo"><a href="#"><img src="../../../assets/icons/logo.svg" alt="logo"></a></div>
          <nav class="header__navigation">
             <ul class="header__nav-list">
               <li class="header__nav-item header__nav-element"><img src="../../../assets/icons/search.svg" alt="search"></li>
               <li class="header__nav-item-line header__nav-element"></li>
               <li class="header__nav-item header__nav-element"><img src="../../../assets/icons/box.svg" alt="box"></li>
               <li class="header__nav-item-line header__nav-element"></li>
               <li class="header__nav-item header__nav-element"><img src="../../../assets/icons/user.svg" alt="user"></li>
             </ul>
          </nav>
          <div class="header__menu-svg"><img src="../../../assets/icons/menu.svg" alt="menu"></div>
        </div>
        <div class="header__content">
            <div class="header__content-img"></div>
            <div class="header__content-info">
               <p class="header__content-moto">Hear it. Feel it</p>
               <h1 class="header__content-heading header__content-heading-big-screen">Move<br> with the<br> music</h1>
               <h1 class="header__content-heading header__content-heading-small-screen">Move with the<br> music</h1>
               <div class="header__content-price">
                  <span class="header__content-price-current header__content-price-num">$ 435</span>
                  <div class="header__content-price-line"></div>
                  <span class="header__content-price-previous header__content-price-num">$ 465</span>
               </div>
               <button class="header__content-price-btn">BUY NOW</button>
            </div>
        </div>
     </div>
  `;

  return header;
}
