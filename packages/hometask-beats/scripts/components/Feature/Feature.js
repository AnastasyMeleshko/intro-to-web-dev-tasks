import "./feature.css";

export default function Feature() {
  const section = document.createElement('section');
  section.classList.add("feature__section");

  section.innerHTML=`
     <div class="container feature__container">
        <h2 class="feature__heading feature__heading-small-screen">Good headphones and<br> loud music is all you<br> need</h2>
        <h2 class="feature__heading feature__heading-big-screen">Good headphones<br>and loud music is all<br> you need</h2>
        <div class="features__items" id="features__items"></div>
        <div class="feature__picture"></div>
     </div>
  `;

  return section;
}


