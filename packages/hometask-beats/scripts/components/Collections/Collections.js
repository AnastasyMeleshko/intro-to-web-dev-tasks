import "./collections.css";

export default function Collections() {
  const section = document.createElement('section');
  section.classList.add("section-collections");

  section.innerHTML=`
     <div class="container section__container" id="about">
       <h2 class="section__collections section__collections-big-screen">Our Latest colour collection 2021</h2>
       <h2 class="section__collections section__collections-small-screen">Our Latest<br> colour collection 2021</h2>
       <div class="section__collections-slider">
          <img src="../../../assets/icons/arrow-left.svg" alt="arrow-left">
          <div class="section__collections-slider-img-left"></div>
          <div class="section__collections-slider-img"></div>
          <div class="section__collections-slider-img-right"></div>
          <img src="../../../assets/icons/arrow-right.svg" alt="arrow-right">
       </div>
     </div>
  `;

  return section;
}
