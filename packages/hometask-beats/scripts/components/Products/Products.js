import "./Products.css";

export default function Products() {
  const section = document.createElement('section');
  section.classList.add("products__section");

  section.innerHTML=`
     <div class="container products__container" id="product">
        <h2 class="products__heading">Our Latest Product</h2>
        <p class="products__text products__text-small-screen">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis nunc ipsum aliquam, ante. </p>
        <p class="products__text products__text-big-screen">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis<br> nunc ipsum aliquam, ante. </p>
        <div class="products__cards" id="products__cards"></div>
     </div>
  `;

  return section;
}


