import "./FeatureItem.css";

export default function FeatureItem(data) {
  const div = document.createElement('div');
  div.classList.add("feature__item");

  div.innerHTML=`
     <div class="feature__item">
       <div class="feature__item-img">
          <img src=${data.img} alt="feature-img">
       </div>
       <div class="feature__item-params">
          <h4 class="feature__item-params-heading">${data.featureText}</h4>
          <p class="feature__item-params-text">${data.featureDescription}</p>
          <a href="#" class="feature__item-params-link">${data.linkText}</a>
       </div>
     </div>
  `;

  return div;
}
