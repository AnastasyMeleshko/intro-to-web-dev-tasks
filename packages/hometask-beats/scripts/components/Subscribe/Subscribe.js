import "./Subscribe.css";

export default function Subscribe() {
  const section = document.createElement('section');
  section.classList.add("subscribe__section");

  section.innerHTML=`
     <div class="container subscribe__container">
        <h2 class="subscribe__container-heading">Subscribe</h2>
        <p class="subscribe__container-text">Lorem ipsum dolor sit amet, consectetur</p>
        <form action="#" class="subscribe__container-form">
            <input type="email" placeholder="Enter Your email address" class="subscribe__container-form-input">
            <button class="subscribe__container-form-btn">Subscribe</button>
        </form>
     </div>
  `;

  return section;
}


