import "./Boxing.css";

export default function Boxing() {
  const section = document.createElement('section');
  section.classList.add("boxing__section");

  section.innerHTML=`
     <div class="container boxing__container">
        <div class="boxing__container-img"></div>
        <div class="boxing__container-info">
            <h2 class="boxing__container-heading boxing__container-heading-big-screen">Whatever you get<br> in the box</h2>
            <h2 class="boxing__container-heading boxing__container-heading-small-screen">Whatever you get in<br> the box</h2>
            <ul class="boxing__container-heading-list">
              <li class="boxing__container-heading-list-item"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle">5A Charger</li>
              <li class="boxing__container-heading-list-item"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle">Extra battery</li>
              <li class="boxing__container-heading-list-item"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle">Sophisticated bag</li>
              <li class="boxing__container-heading-list-item"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle">User manual guide</li>
            </ul>
        </div>
     </div>
  `;

  return section;
}


