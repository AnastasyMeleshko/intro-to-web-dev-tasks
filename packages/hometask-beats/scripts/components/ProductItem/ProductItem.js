import "./ProductItem.css";

export default function ProductItem(data) {
  const div = document.createElement('div');
  div.classList.add("product__item");

  div.innerHTML=`
     <div class="product__item">
       <div class="product__bg product__blue">
         <img class="product__item-img" src="${data.img}" alt="headsets-image">
       </div>
       <div class="product__card-icon">
         <img src="../../../assets/icons/shopping-cart.svg" alt="card-icon">
       </div>
       <div class="product__item-data">
         <div class="product__item-data-row">
            <div class="product__item-data-rating" id="product__item-data-rating">
                 <img src="../../../assets/icons/star.svg" alt="star">
                 <img src="../../../assets/icons/star.svg" alt="star">
                 <img src="../../../assets/icons/star.svg" alt="star">
                 <img src="../../../assets/icons/star.svg" alt="star">
                 <img src="../../../assets/icons/star.svg" alt="star">
           </div>
           <p class="product__item-data-text">${data.rating}</p>
         </div>
           <div class="product__item-data-row">
            <p class="product__item-data-text">${data.itemName}</p>
            <p class="product__item-data-price">${data.price}</p>
         </div>
       </div>
     </div>
  `;

  return div;
}
