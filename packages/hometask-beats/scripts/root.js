import Header from "./components/Header/Header";
import Main from "./components/Main";
import insertFeatures from "./functions/insertFeatures";
import insertProducts from "./functions/insertProducts";
import Footer from "./components/Footer/Footer";

const App = document.querySelector(`.app`);

App.append(Header());
App.append(Main());
insertFeatures();
insertProducts();
App.append(Footer());

