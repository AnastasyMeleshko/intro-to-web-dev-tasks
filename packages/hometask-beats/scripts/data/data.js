// eslint-disable-next-line import/prefer-default-export
export const FEATURES = [
  {
    img: "../../assets/icons/battery.svg",
    featureText: "Battery",
    featureDescription: "Battery 6.2V-AAC codec",
    linkText: "Learn More"
  },
  {
    img: "../../assets/icons/bluetooth.svg",
    featureText: "Bluetooth",
    featureDescription: "Battery 6.2V-AAC codec",
    linkText: "Learn More"
  },
  {
    img: "../../assets/icons/microphone.svg",
    featureText: "Microphone",
    featureDescription: "Battery 6.2V-AAC codec",
    linkText: "Learn More"
  }
]

export const PRODUCTS = [
  {
    img: "../../assets/images/product-03.png",
    rating: "4.50",
    itemName: "Red Headphone",
    price: "$ 256",
    colorBg: "var(--color-pink)",
    id: "3"
  },
  {
    img: "../../assets/images/product-02.png",
    rating: "4.50",
    itemName: "Blue Headphone",
    price: "$ 235",
    colorBg: "var(--color-blue)",
    id: "1"
  },
  {
    img: "../../assets/images/product-01.png",
    rating: "4.50",
    itemName: "Green Headphone",
    price: "$ 245",
    colorBg: "var(--color-green)",
    id: "2"
  }
]
