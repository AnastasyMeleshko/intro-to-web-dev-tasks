import ProductItem from "../components/ProductItem/ProductItem";
import {PRODUCTS} from "../data/data";

export default function insertProducts() {
  const productsElems = document.getElementById("products__cards");

  PRODUCTS.forEach(product => {
    const productItem = ProductItem(product);
    productItem.setAttribute("id",`product-item-${product.id}`)
    productItem.children[0].children[0].style.backgroundColor = `${product.colorBg}`;
    productItem.children[0].children[1].style.backgroundColor = `${product.colorBg}`;
    productsElems.appendChild(productItem);
  });
}

