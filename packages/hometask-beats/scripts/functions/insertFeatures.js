import FeatureItem from "../components/FeatureItem/FeatureItem";
import { FEATURES } from "../data/data";

export default function insertFeatures() {
  const featuresElems = document.getElementById("features__items");

  FEATURES.forEach(feature => {
    const featureItem = FeatureItem(feature);
    featuresElems.appendChild(featureItem);
  });
}

