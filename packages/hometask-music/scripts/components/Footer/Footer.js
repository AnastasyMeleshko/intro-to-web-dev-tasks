import "./Footer.css";

export default function Footer() {
  const footer = document.createElement('footer');
  footer.classList.add("footer");

  footer.innerHTML = `
     <div class="container footer__container">
        <nav class="footer__container__navigation">
          <ul class="footer__container__navigation-list">
            <li class="footer__container__navigation-item">About As</li>
            <li class="footer__container__navigation-item">Contact</li>
            <li class="footer__container__navigation-item">CR Info</li>
            <li class="footer__container__navigation-item">Twitter</li>
            <li class="footer__container__navigation-item">Facebook</li>
          </ul>
        </nav>
        <div class="footer__container-tag">Wlodzimierz Simon © 2022</div>
     </div>
  `;

  return footer;
}

