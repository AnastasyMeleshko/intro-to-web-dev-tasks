import "./Landing.css";

export default function Landing() {
  const div = document.createElement('div');
  div.classList.add("landing-page");

  div.innerHTML = `
     <div class="container landing__container">
        <div class="landing__content" id="landing__content">
            <h1 class="landing__content-heading">Feel the music</h1>
            <p class="landing__content-text">Stream over 10 million songs with one click</p>
        </div>
        <div class="landing-img"></div>
     </div>
  `;

  return div;
}

