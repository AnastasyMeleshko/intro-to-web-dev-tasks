import "./Main.css";

export default function Main() {
  const main = document.createElement('main');
  main.classList.add("main");

  main.innerHTML = `
     <div class="container main__container" id="main__container"></div>
  `;

  return main;
}

