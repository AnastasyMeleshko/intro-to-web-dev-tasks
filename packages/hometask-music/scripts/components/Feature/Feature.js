import "./Feature.css";

export default function Feature() {
  const div = document.createElement('div');
  div.classList.add("feature");

  div.innerHTML = `
     <div class="container feature__container">
        <div class="feature__content">
            <h2 class="feature__content-heading">Discover new music</h2>
            <div class="feature__content-buttons" id="feature__content-buttons"></div>
            <p class="feature__content-text">By joing you can benefit by listening to the latest albums released</p>
        </div>
        <div class="feature__image"></div>
     </div>
  `;

  return div;
}
