import "./Header.css";

export default function Header() {
  const header = document.createElement('header');
  header.classList.add("header");

  header.innerHTML = `
     <div class="container header__container">
       <div class="header__container-logo"><a href="#"><img src="../../../assets/icons/logo.svg" alt="logo"></a></div>
       <nav class="header__container__navigation">
         <ul class="header__container__navigation-list">
           <li class="header__container__navigation-list-item" id="landing">Simo</li>
           <li class="header__container__navigation-list-item" id="discover">Discover</li>
           <li class="header__container__navigation-list-item" id="join">Join</li>
           <li class="header__container__navigation-list-item" id="sign-up">Sign In</li>
         </ul>
       </nav>
     </div>
  `;

  return header;
}
