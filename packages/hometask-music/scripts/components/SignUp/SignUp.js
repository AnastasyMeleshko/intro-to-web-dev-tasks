import "./SignUp.css";

export default function SignUp() {
  const div = document.createElement('div');
  div.classList.add("sign-up");

  div.innerHTML = `
     <div class="container sign-up__container">
        <div class="sign-up-content" id="sign-up-content">
           <form action="#" class="sign-up__form">
             <div class="sign-up__form-block">
              <label for="name">Name:</label>
              <input type="text" id="name" name="name" required>
             </div>
             <div class="sign-up__form-block">
              <label for="password">Password:</label>
              <input type="password" id="password" name="password" required>
             </div>
             <div class="sign-up__form-block">
              <label for="email">e-mail:</label>
              <input type="email" id="email" name="email" required>
             </div>
           </form>
        </div>
        <div class="sign-up-img"></div>
     </div>
  `;

  return div;
}
