import "./Button.css";

export default function Button(text, className) {
  const button = document.createElement('button');
  button.classList.add(className);
  button.classList.add("common-button");
  button.innerHTML = `${text}`;

  return button;
}
