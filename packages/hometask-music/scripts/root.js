import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Main from "./components/Main/Main";
import Landing from "../scripts/components/Landing/Landing";
import Feature from "../scripts/components/Feature/Feature";
import SignUp from "../scripts/components/SignUp/SignUp";
import Button from "./components/Button/Button";

const App = document.getElementById('app');

App.append(Header());
App.append(Main());
App.append(Footer());

//add main initial content
const main = document.getElementById('main__container');
main.append(Landing());
document.getElementById("landing__content").append(Button("Join now", "landing__content-btn"))

//router for main content
export default function showPage(e) {

  if ((e.target.id === 'discover') && (main.children[0].classList[0] === 'landing-page')) {
    main.innerHTML = '';
    main.append(Feature());
    let arr = [{
      text: "Charts",
      class: "discover__button",
    },
      {
        text: "Songs",
        class: "discover__button"
      },
      {
        text: "Artists",
        class: "discover__button"
      }
    ];
    arr.forEach(el => document.getElementById("feature__content-buttons").append(Button(el.text, el.class)))
  }

  if ((e.target.id === 'discover') && (main.children[0].classList[0] === 'sign-up')) {
    main.innerHTML = '';
    main.append(Landing());
    document.getElementById("landing__content").append(Button("Join now", "landing__content-btn"));

  }

  if (e.target.id === 'landing') {
    main.innerHTML = '';
    main.append(Landing());
    document.getElementById("landing__content").append(Button("Join now", "landing__content-btn"));
  }

  if ((e.target.id === 'join') || (e.target.id === 'sign-up')) {
    main.innerHTML = '';
    main.append(SignUp());
    document.getElementById("sign-up-content").append(Button("Join now", "sign-up__button"));

  }

}

document.body.addEventListener("click", showPage);
